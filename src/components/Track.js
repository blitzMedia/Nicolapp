import React from 'react'
import ReactPlayer from 'react-player'
import { TrackItem, TrackImg, TrackTitle } from '../assets/styledComps'

const Track = ({
  release, isActive = false,
  track: { id, title, slug, portada, url }
}) => (
  <TrackItem to={`${release}/${slug}`} key={id} active={isActive ? isActive : undefined} >
    <TrackTitle>{title}</TrackTitle>

    <TrackImg bg={portada.fixed.src} />

    { isActive && <ReactPlayer style={{ maxWidth: '100%', height: '100%' }} url={url} /> }
  </TrackItem>
)

export default Track
