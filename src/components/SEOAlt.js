import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"

// Complete list of exports
import Html, {
  Metadata,
  withMetadata,
  HtmlTag,
  HeadTag,
  BodyTag,
  METADATA_PROP_NAME
} from 'react-html-metadata'

const metadata = Metadata.createNew({
  title: 'Html Metadata Demo',
  htmlAttributes: { lang: 'en' },
  bodyAttributes: { className: 'root' },
  meta: [
    { charset: 'utf-8' }
  ],
  link: [
    { rel: 'stylesheet', href: '/static/css/app.css' }
  ]
})

function SEO({ description, lang, meta, keywords, title, path }) {
  return (
    <StaticQuery
      query={detailsQuery}
      render={( { site: { siteMetadata } } ) => {
        let superTitle = title ? title : siteMetadata.title,
            titleTemplate = path === '/' ? title : siteMetadata.titleTemplate,
            metaDescription = description || siteMetadata.description

        return (
          <Html metadata="metadata"></Html>
        )
      }}
    />
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  keywords: [],
  title: ''
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.array,
  keywords: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
}

export default SEO

const detailsQuery = graphql`
  query SEOAltQuery {
    site {
      siteMetadata {
        title
        titleTemplate
        description
        url
        OGImage
      }
    }
  }
`
