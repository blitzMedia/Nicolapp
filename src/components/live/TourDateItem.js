import React from 'react'
import moment from 'moment'
import { Tr, Td } from 'react-super-responsive-table'

const TourDateItem = ({event}) => {
  let date = event.start.datetime !== null
              ? moment(new Date(event.start.datetime))
              : moment(new Date(event.start.date))

  const Title = event.displayName && (
    <Td className="title">
      {event.displayName}
    </Td>
  )

  const Dater = (
    <Td className="date">
      <p>{moment(date).format('LL')}</p>
      {event.start.datetime && (
        <p>{moment(date).format('hh:mm a')}</p>
      )}
    </Td>
  )

  const Venue = (
    <Td className="venueAndLocation">
      <p>{event.venue.displayName}</p>
      <p>{event.location.city}</p>
    </Td>
  )

  const Tickets = event.uri && (
    <Td className="tickets">
      <p>
        <a href={event.uri} rel="noopener noreferrer" target="_blank">Get Tickets</a>
      </p>
    </Td>
  )

  return(
    <Tr key={event.id}>
        {Title}

        {Dater}

        {Venue}

        {Tickets}
    </Tr>
  )
}

export default TourDateItem
