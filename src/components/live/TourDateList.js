import React from 'react'
import { Table, Thead, Tbody, Tr, Th } from 'react-super-responsive-table'
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css'
import TourDateItem from './TourDateItem'

const TourDateList = (props) => {
  const tourDateItems = props.events.map(ev => <TourDateItem event={ev} key={ev.id} />)

  return(
    <Table>
      <Thead>
        <Tr>
          <Th>Name</Th>
          <Th>Date</Th>
          <Th>Venue</Th>
          <Th>Tickets</Th>
        </Tr>
      </Thead>
      <Tbody>{ tourDateItems }</Tbody>
    </Table>
  )
}

export default TourDateList
