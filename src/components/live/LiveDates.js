import React from 'react'
import TourDateList from './TourDateList'
import Layout from '../layout'
import { LiveDates } from '../../assets/styledComps'

const artist_id = '5205293-nicola-cruz'
const endpoint = `https://api.songkick.com/api/3.0/artists/${artist_id}/calendar.json?apikey=${process.env.GATSBY_SONGKICK}`

class Live extends React.Component {
  state = { events: [] }

  async componentDidMount() {
    try {
      let response = await fetch(endpoint)
      let data = await response.json()
      const events = data.resultsPage.results.event

      this.setState({events})

    } catch(err) {
      console.log('ERROR fetching events:', err)
    }
  }

  render() {
    const {events} = this.state || []

    return (
      <LiveDates>
        <Layout center title="Shows">
          <TourDateList events={events} />
        </Layout>
      </LiveDates>
    )
  }
}

export default Live
