import React from "react"
import { GlobalStyles, SuperSection, SuperImg } from '../assets/styledComps'
import BackHome from '../components/BackHome'
import SEO from '../components/SEO'

export default ({ portada, children, center, title }) => {
  return (
  <>
    <GlobalStyles />
    <BackHome />
    <SEO title={title} />

    <SuperSection center={center}>
      { portada && <SuperImg overlay fluid={portada} /> }

      {children}
    </SuperSection>
  </>
)}
