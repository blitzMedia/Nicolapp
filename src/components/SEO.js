import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import Helmet from 'react-helmet'

function SEO({ description, title, path }) {
  return (
    <StaticQuery
      query={detailsQuery}
      render={( { site: { siteMetadata } } ) => {
        let superTitle = title ? title : siteMetadata.title,
            metaDescription = description || siteMetadata.description
            //titleTemplate = path === '/' ? title : siteMetadata.titleTemplate;

        return (
          <Helmet>
            <title itemProp="name">{superTitle}</title>
            <meta name="title" content={superTitle} />
            <meta name="description" content={metaDescription} />
            <meta property="og:type" content="website" />
            <meta property="og:title" content="Nicola Cruz" />
            <meta property="og:image" content={`${siteMetadata.url}${siteMetadata.OGImage}`} />
            <meta property="og:type" content="website" />
            <meta name="twitter:card" content="summary" />
            <meta name="twitter:creator" content="gorkamolero" />
            <meta name="twitter:description" content={metaDescription} />
          </Helmet>
        )
      }}
    />
  )
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
  keywords: [],
  title: ''
}

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.array,
  keywords: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired,
}

export default SEO

const detailsQuery = graphql`
  query DefaultSEOQuery {
    site {
      siteMetadata {
        title
        titleTemplate
        description
        url
        OGImage
      }
    }
  }
`
