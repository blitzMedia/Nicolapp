import React from 'react'
import {SuperNav, NavBlock, Nicola} from '../assets/styledComps'

const BackHome = () => (
  <SuperNav>
    <NavBlock direction="leftTop" className="logo">
      <Nicola>
        <span>Nicola</span>
        <span>Cruz</span>
      </Nicola>
    </NavBlock>

    <NavBlock
      noexpand
      direction="rightTop"
      onClick={() => window.history.back()}
      className="closeBtn"
    >
      <span style={{fontSize: '300%'}}>×</span>
    </NavBlock>
  </SuperNav>
)

export default BackHome
