import React from 'react'
import LiveDates from '../components/live/LiveDates'
import { GlobalStyles } from '../assets/styledComps'

const LivePage = () => (
  <>
    <GlobalStyles />
    <LiveDates />
  </>
)

export default LivePage
