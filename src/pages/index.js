import React from 'react'
import { graphql } from 'gatsby'
import Link from 'gatsby-link'
import SEO from '../components/SEO'
import {
  GlobalStyles,
  ReleaseLogo,
  SuperImg,
  SuperDiv,
  AlbumLogo,
  ReleaseTitle,
  ReleaseBody,
  SuperNav,
  Nicola,
  NavBlock
} from '../assets/styledComps'
import 'typeface-saira-extra-condensed'
import Parallax from 'parallax-js'

class Release extends React.Component {
  createMarkup = (content) => { return {__html: content || ''}; }
  hello = () => { console.log('hey') }
  render() {
    console.log(this)
    const {
      node: {
        slug,
        title,
        portada,
        textos = [],
        buyLink = '',
        releaseLogo,
        releaseLogo: {
          file: {
            url: imago,
            fileName: alt
          }
        },
        childContentfulReleaseExtraRichTextNode: { childContentfulRichText: { html } }
      }
    } = this.props

    const father = 0.3, son = 0.1

    const releaseSuperTitle = releaseLogo
          ? (<ReleaseLogo src={imago} alt={alt} />)
          : (<ReleaseTitle>{title}</ReleaseTitle>)


    if (buyLink === '' && textos.length === 0) return null

    else return (
      <>
        <SuperNav>

          {
            buyLink !== '' && (
              <NavBlock direction="rightBottom">
                <a  href="https://nicolacruz.lnk.to/siku"
                    target="_blank"
                    rel="noreferrer noopener">二  Order now</a>
              </NavBlock>
            )
          }

          {
            textos.length > 0 && (
              <NavBlock direction="leftBottom">
                <Link to={`/${title.toLowerCase()}/notes`}>三 Notes</Link>
              </NavBlock>
            )
          }
        </SuperNav>

        <main key={slug} className="release">
          <SuperDiv style={{overflow: 'hidden'}} id="parallax"
              data-friction-x="0.1" data-friction-y="0.1"
              data-scalar-x="25" data-scalar-y="15">

            <div className="parallax-window full" data-depth={father/20}>
              <div className="parallax-slider full">
                <SuperImg fluid={portada.fluid} />
              </div>
            </div>

            <div className="parallax-window full" data-depth={-(son/6)}>
              <div className="parallax-slider full center">
                <div className="glass" onClick={this.hello}>
                  <AlbumLogo to={slug}>
                    <div className="center column">
                      {releaseSuperTitle}
                      {html && <ReleaseBody dangerouslySetInnerHTML={this.createMarkup(html)} />}
                    </div>
                  </AlbumLogo>
                </div>
              </div>
            </div>
          </SuperDiv>
        </main>
      </>
    )
  }
}

class IndexPage extends React.Component {
  componentDidMount() {
    // Evaluamos parallax
    const bigEnough = window.matchMedia( "(min-width: 1024px)" )
    bigEnough.matches && new Parallax(document.querySelector('#parallax'))
  }
  render() {
    const { edges = [] } = this.props.data.allContentfulRelease
    return (
      <>
        <SEO path={this.props.uri} />
        <SuperNav>
          <NavBlock direction="leftTop" className="logo">
            <Nicola>
              <span>Nicola</span>
              <span>Cruz</span>
            </Nicola>
          </NavBlock>

          <NavBlock direction="rightTop">
            <Link to="/shows">一 Shows</Link>
          </NavBlock>
        </SuperNav>

        <GlobalStyles />
        {
          edges.filter(({node}) => node.node_locale === 'en-US').map(({node}) =>(
            <Release key={node.slug} node={node} id={node.id} />
          ))
        }
      </>
    )
  }
}

export default IndexPage


// Optional: ...GatsbyContentfulFluid_tracedSVG
export const pageQuery = graphql`
	query pageQuery {
		allContentfulRelease {
			edges {
				node {
					title
          slug
          releaseLogo { file { url } }
					portada {
						fluid(maxWidth: 2048) {
							aspectRatio
							sizes
							src
							srcSet
						}
          }
          textos { id }
          buyLink
          childContentfulReleaseExtraRichTextNode {
            childContentfulRichText { html }
          }
          node_locale
				}
			}
		}
	}
`
