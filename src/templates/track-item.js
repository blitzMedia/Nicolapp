import React, { Component} from 'react'
import ReactPlayer from 'react-player'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import FlexContainer from 'react-styled-flexbox'

import Layout from '../components/layout'
import { ReactPlayerWrapper } from '../assets/styledComps'

class TrackItem extends Component {
	render() {
    console.log(111, this.props)
    const { title, url } = this.props.data.contentfulTrack
    const { portada } = this.props.data.contentfulTrack.release[0]

		return (
			<Layout title={title} portada={portada.fluid}>
        <FlexContainer className="playerContainer" itemsCenter justifyCenter directionColumn>
          <ReactPlayerWrapper>
            <ReactPlayer
              className="react-player"
              url={url}
              playing
              width="100%"
              height="100%"
            />
          </ReactPlayerWrapper>
        </FlexContainer>
			</Layout>
		)
	}
}

TrackItem.propTypes = {
  data: PropTypes.object.isRequired
}

export default TrackItem

export const pageQuery = graphql`
	query trackItemQuery($slug: String!) {
		contentfulTrack(slug: {eq: $slug}) {
			title
      url
      release {
        portada {
          fluid(maxWidth: 2048) {
            aspectRatio
            sizes
            src
            srcSet
          }
        }
      }
		}
	}
`
