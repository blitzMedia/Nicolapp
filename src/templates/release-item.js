import React, { Component} from 'react'

import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import Grid from 'react-css-grid'

import Layout from '../components/layout'
import Track from '../components/Track'

class ReleaseItem extends Component {

	render() {
		const { title, slug, tracks, portada } = this.props.data.contentfulRelease

		const trackList = tracks.map(track => {
      const isActive = track.id === this.props.pageContext.activeTrack

      return (
        <Track release={slug} track={track} key={track.id} isActive={isActive} />
      )
    })

		return (
      <Layout portada={portada.fluid} title={title}>
        <div className="superGrid" style={{ padding: '160px 12vw 100px' }}>
          <Grid width={240} gap={80}>
            {trackList}
          </Grid>
        </div>
      </Layout>
		)
	}
}

ReleaseItem.propTypes = {
  data: PropTypes.object.isRequired
}

export default ReleaseItem

export const pageQuery = graphql`
	query releaseItemQuery($slug: String!) {
		contentfulRelease(slug: {eq: $slug}) {
			title
      slug
      portada {
        fluid(maxWidth: 2048) {
          aspectRatio
          sizes
          src
          srcSet
        }
      }
			tracks {
        id
				title
				slug
				url
				portada {
					fixed(width: 420) {
            width
						src
						srcSet
					}
				}
      }
      childContentfulReleaseExtraRichTextNode {
        childContentfulRichText {
          html
        }
      }
		}
	}
`
//...GatsbyContentfulFluid_tracedSVG
