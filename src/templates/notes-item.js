import React, { Component} from 'react'
import PropTypes from 'prop-types'
import { graphql } from 'gatsby'
import FlexContainer from 'react-styled-flexbox'
import ReactMarkdown from 'react-markdown'

import Layout from '../components/layout'
import { SuperFooter, SuperLetters, ArticleTitle } from '../assets/styledComps'


class NotesItem extends Component {
  createMarkup = (content) => { return {__html: content || ''}; }
	render() {
    const { title, portada, textos } = this.props.data.contentfulRelease

		return (
			<Layout title={`${title} - Notes`} portada={portada.fluid}>
        <SuperLetters text={title}>
          <FlexContainer style={{ padding: '150px 2vw' }}>
            {
              textos.map(({
                title,
                childContentfulTextsMarkdownTextNode: { markdown }
              }) => {
                return (
                  <article key={title}>
                    <ArticleTitle>{title}</ArticleTitle>
                    <ReactMarkdown source={markdown} />
                  </article>
                )
              })
            }

            <SuperFooter>
              <span>Powered by  <a href="https://qtzlctl.com" target="_blank" rel="noopener noreferrer" style={{ fontWeight: 'bold' }}>Q T Z L</a></span>
              <span> | </span>
              <span><a href="https://laluqueria.com" target="_blank" rel="noopener noreferrer" style={{ fontWeight: 'bold' }}>La Luquería</a></span>
              <span> | </span>
              <span><a href="https://blitz.media" target="_blank" rel="noopener noreferrer" style={{ fontWeight: 'bold' }}>Blitz! Media</a></span>
            </SuperFooter>
          </FlexContainer>
        </SuperLetters>
			</Layout>
		)
	}
}

NotesItem.propTypes = {
  data: PropTypes.object.isRequired
}

export default NotesItem

export const pageQuery = graphql`
	query noteItemQuery($slug: String!) {
		contentfulRelease(slug: {eq: $slug}) {
			title
      textos {
        title
        childContentfulTextsMarkdownTextNode {
          markdown
        }
      }
      portada {
        fluid(maxWidth: 2048) {
          aspectRatio
          sizes
          src
          srcSet
        }
      }
		}
	}
`
