import { createGlobalStyle } from 'styled-components'
import styledNormalize from 'styled-normalize'
import styled from 'styled-components'
import Link from 'gatsby-link'
import Img from 'gatsby-image'
import texture from './texture.png'

export const GlobalStyles = createGlobalStyle`
  ${styledNormalize}

  body {
    background: black;
    color: white;

    font-family: 'Saira Extra Condensed';
    font-size: 18px;
    font-weight: lighter;
    letter-spacing: .05em;
  }

  //main:not(:first-of-type) { display: none; }

  h1, h2, h3, h4, h5, h6 {
    font-family: inherit;
    color: inherit;
    text-transform: uppercase;
    letter-spacing: .5rem;
  }

  a, a:active {
    text-decoration: none !important;
    color: inherit;
  }

  table {
    width: 100%;
    thead th {
      border-bottom: 1px solid white !important;
      padding-bottom: .75em !important;
    }
    td, th {
      text-align: left;
      padding: .25em;
      font-weight: lighter;
    }
    tr { padding: 1em; }
    tbody tr:first-child td { padding-top: 1em; }
    .title {
      font-size: 110%;
      font-weight: normal;
    }
    @media screen and (max-width: 40em) {
      tr {
        padding: 1em !important;
        border-bottom: 1px solid rgba(255, 255, 255, .8) !important;
      }
    }
    a { font-weight: bold; }
  }

  .player { width: 100% !important; }
  .full { height: 100vh; width: 100vw; }
  .center {
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .full.center {
    background: rgba(0,0,0,.25);
  }
  .column { flex-direction: column; }

  // Parallax
  #parallax {
    pointer-events: inherit !important;
  }
  .parallax-window { position: absolute; }
  .glass {
    transition: filter .25s ease;
    &:hover { filter: blur(0.5px); }
  }

  // Table
  @media screen and (max-width: 40em) {
    table.responsiveTable {
      td {
        .tdBefore { display: none !important; }
        &.pivoted { padding-left: 0 !important; }
      }
    }

    .superGrid {
      padding-top: 120px !important;
      padding-left: 4vw !important;
      padding-right: 4vw !important;
    }
    .closeBtn {
      right: -6vw !important;
    }
  }

  .playerContainer {
    max-width: calc(680px - 8vw);
    margin: 0 auto;
    height: 100vh;
    padding: 0 4vw;
  }
`

export const ReleaseLogo = styled.img`
  max-width: 170px;
`

export const SuperLink = styled(Link)`
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  color: white;
  overflow: hidden;
`

export const SuperDiv = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  color: white;
`

export const SuperSection = styled.section`
  min-height: 100vh;
  position: relative;
  color: white;
  overflow: hidden;

  ${({center}) => center && `
    display: flex;
    align-items: center;
    justify-content: center;
  `};
`

export const SuperImg = styled(Img)`
  position: absolute !important;
  right: 0; bottom: 0;
  left: 0; bottom: 0;
  width: 100%; height: 100%;
  @media (min-width: 1024px) {
    left: -0.5%; top: -0.5%;
    width: 101%; height: 101%;

  }
  // Hack
  @media (max-width: 1024px) and (orientation: portrait) {
    //width: 103%
  }
  z-index: -1;

  &:after {
    content: '';
    background: ${props => props.overlay ? "rgba(0,0,0,.75)" : "rgba(0,0,0,0)"} ;
    position: absolute !important;
    left: 0; top: 0; right: 0; bottom: 0;
    width: 100%; height: 100%;
    z-index: 0;
  }

  @media (max-width: 730px) {

  }
`

export const ReleaseTitle = styled.h1`
  bottom: 0; left: 0;
  padding: 0 80px;
  font-weight: lighter;
  user-select: none;

  position: ${props => props.fixed ? "fixed" : "static"};
`

export const ReleaseBody = styled.aside`
  display: block;
`

export const AlbumLogo = styled(Link)`
  text-align: center;
  padding: 2em;

  //background-image: url(${texture});
  //background-size: contain;

  transition: background-color 1s ease;
  background: rgba(255, 255, 255, .05);
  &:hover { background: rgba(255, 255, 255, .1) }


  /* Super anim */
  @keyframes pulse {
    0% { background-color: rgba(255, 255, 255, .05); }
    50% { background-color: rgba(255, 255, 255, .15); }
    100% { background-color: rgba(255, 255, 255, .05); }
  }

  @media screen and (max-width: 60em) {
    animation: pulse .5s 3 ease-in-out;
  }

  border-radius: 100%;
  width: 200px;
  height: 200px;

  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  h1, p { margin: 0; }
  p { margin: .5em 0; }
`

export const TrackImg = styled.div`
  position: absolute;
  left: 0; top: 0;
  width: 100%; height: 100%;
  z-index: -1;
  background: ${props => `url(${props.bg}) no-repeat top center`};
  background-size: cover !important;
`

export const TrackTitle = styled.h2`
  color: white;
  text-decoration: none !important;
  font-size: 320%;

  font-weight: lighter;
  text-transform: lowercase;
  letter-spacing: .05em;
  line-height: 1;
  margin: 0;
  padding: 1em;

  position: absolute;
  bottom: 0;  left: 0;
  transform: rotate(270deg) translate(-100px, calc(90%));
  transform-origin: bottom left;
  transition: transform .25s ease;
`

export const TrackItem = styled(Link)`
  min-height: 60vh;
  @media screen and (max-width: 40em) { min-height: 50vh; }
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  box-shadow: 0 19px 38px rgba(0,0,0,0.15), 0 15px 12px rgba(0,0,0,0.11);

  &:before {
    content: '';
    position: absolute;
    left: 0; top: 0;
    width: 100%; height: 100%;
    z-index: 0;
    @media screen and (min-width: 40em) { background: rgba(0, 0, 0, .5); }
  }

  transition: transform .15s ease;

  ${props => props.active ? `
      ${TrackImg} { opacity: 1; }
      transform: scale(1.05);

      ${TrackTitle} {
        transform: rotate(270deg) translate(-30%, 100%);
        letter-spacing: .75em;
      }
    ` : ``
  }

  &:hover {
    transform: scale(1.02);

    ${props => props.active ? `
      ${TrackImg} { opacity: 1; }
      transform: scale(1.05);
    ` : ``}
  }
`

export const LiveDates = styled.section`
  width: 100vw; height: 100vh;
  display: flex;
  align-items: flex-start;
  justify-content: center;

  table { max-width: 800px; }
  p { margin: 0; }
  div { color: inherit !important; }
  [data-tag] { pointer-events: none; }


  // Shows stuff
  section { padding: 8em 3vw; }
  section:before {
    content: 'shows';
    font-size: 14em;
    font-weight: lighter;
    letter-spacing: 15px;
    position: absolute;
    left: 0;  z-index: 0;

    transform: rotate(-90deg) translate(-50%,0%);
    transform-origin: center left;

    opacity: .25;
  }
  table {
    position: relative;
    z-index: 1;
    background: black;
  }

  @media screen and (max-width: 40em) {
    section { padding: 6em 4vw; }
    table.responsiveTable tr { padding: 0 !important; }
  }
`

export const SuperNav = styled.nav`
  //position: fixed; top: 0; left: 0; right: 0; bottom: 0;
  //width: 100vw; height: 100vh;
`

export const NavBlock = styled.div`
  font-size: 110%;
  cursor: pointer;
  position: absolute;
  z-index: 2;
  padding: 4vw;

  &.logo { opacity: 1; }

  ${({noPointer}) => noPointer && `pointer-events: none;`}

  ${({direction}) => direction === 'leftTop' && ` top: 0; left: 0; `}
  ${({direction}) => direction === 'rightBottom' && ` right: 0; bottom: 0; `}
  ${({direction}) => direction === 'rightTop' && `
    top: 0; right: 0;
    transform: rotate(90deg) translate(100%, 0%);
    transform-origin: top right;
  `}
  ${({direction}) => direction === 'leftBottom' && `
    bottom: 0; left: 0;
    transform: rotate(270deg) translate(0%,100%);
    transform-origin: bottom left;
  `}

  // Op effect
  transition: opacity .25s ease-out, letter-spacing .25s ease-out;
  letter-spacing: 0.05em;
  opacity: .9;
  &:hover {
    opacity: 1;
    letter-spacing: ${({noexpand}) => noexpand ? '0' : '.2em' }
  }
`

export const Nicola = styled.h1`
  margin: 0;
  text-transform: lowercase;
  font-weight: normal;
  font-size: 260%;
  letter-spacing: 0;
  line-height: .8;
  span { display: block; }

  @media (max-width: 600px) {
    font-size: 210%;
    opacity: .8;
  }
`

export const SuperFooter = styled.footer`
  position: fixed;
  bottom: 0; left: 0;
  width: 100%;
  text-align: center;
  padding: 1em;
  &:before {
    content: '';
    position: absolute;
    top: 0; right: 0; bottom: 0; left: 0;
    z-index: -1;
    background: rgba(0,0,0,.8);
  }
`

export const SuperLetters = styled.div`
  max-width: 768px;
  margin: 0 auto;
  position: relative;

  article {
    padding: 0 15px;
    &:before {
      content: '';
      position: absolute;
      top: 0; right: 0; bottom: 0; left: 0;
      z-index: -1;
      background: rgba(0,0,0,.25);
      filter: blur(5px);
    }
  }


  &:before {
    content: ${({text}) => `'${text}'`};
    font-size: 14em;
    font-weight: lighter;
    text-transform: lowercase;
    letter-spacing: 15px;
    position: absolute;
    left: 0;  z-index: -1;

    transform: rotate(-90deg) translate(-100%, -10%);
    transform-origin: center left;

    opacity: .25;
  }

  &:after {
    position: absolute;
    top: 0; right: 0; bottom: 0; left: 0;
    width: 100%; height: 100%;
    background: black;
    z-index: 1;
  }

  ${({bg}) => `
    > * { background: ${bg}; }
  `}
`

export const ArticleTitle = styled.p`
  text-align: right;
  font-weight: bold;
  margin-bottom: -.3em;

  @media screen and (max-width: 60em) {
    text-align: left;
  }
`

export const ReactPlayerWrapper = styled.article`
  position: relative;
  padding-bottom: 56.25%;
  width: 100%;

  .react-player {
    position: absolute;
    top: 0; left: 0;
  }
`
