const dotenv = require("dotenv")
const path = require('path')

if (process.env.ENVIRONMENT !== "production") dotenv.config()
const { spaceId, accessToken } = process.env

const production = process.env

module.exports = {
  plugins: [
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId,
        accessToken
      }
    },
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-favicon`,
    `@contentful/gatsby-transformer-contentful-richtext`,
    `gatsby-plugin-styled-components`
  ],
  siteMetadata: {
    title: "Nicola Cruz",
    author: "Gorka from QTZL",
    titleTemplate: "%s · Nicola Cruz",
    description: "Official site for Nicola Cruz.",
    url: "https://nicolacruz.com", // No trailing slash allowed!
    OGImage: "/nicolapp.png"
  },
};
//url: "https://nicolapp.netlify.com", // No trailing slash allowed!
