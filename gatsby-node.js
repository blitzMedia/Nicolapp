const path = require('path')

exports.createPages = ({graphql, actions}) => {
  const {createPage} = actions

  return new Promise((resolve, reject) => {
    const releaseItemTemplate = path.resolve('./src/templates/release-item.js')
    const trackItemTemplate = path.resolve('./src/templates/track-item.js')
    const notesItemTemplate = path.resolve('./src/templates/notes-item.js')

    resolve(
      graphql(`
        {
          allContentfulRelease(filter: {node_locale: {eq: "en-US"}}) {
            edges {
              node {
                id
                slug
                node_locale
                tracks {
                  id
                  title
                  slug
                }
              }
            }
          }
        }
      `).then(result => {
        if (result.errors) reject(result.errors)
        const releases = result.data.allContentfulRelease.edges

        releases.forEach(({ node: {
          slug,
          tracks,
          node_locale,
          textos
        } }) => {

          if(node_locale !== 'en-US') return;

          createPage ({
            path: slug,
            component: releaseItemTemplate,
            context: { slug: slug }
          })

          createPage({
            path: `${slug}/notes`,
            component: notesItemTemplate,
            context: { slug }
          })

          tracks.forEach(track => {
            createPage({
              path: `${slug}/${track.slug}`,
              component: trackItemTemplate,
              context: { slug: track.slug }
            })
          })
        })
        return
      })
    )
  })
}
